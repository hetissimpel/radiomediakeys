RadioMediaKeys
==============

Introduction
------------

This is a simple source code example that demonstrates controlling [Radio.app](http://radioformac.com) using the media keys on an Apple keyboard.

How it works
------------

[Radio.app](http://radioformac.com) exposes a very simple Apple Script interface and RadioMediaKeys sends AppleScript to Radio when a media key is pressed.

AppleScript interface
---------------------

In this version of [Radio.app](http://radioformac.com) the following AppleScript is supported:

`tell application "Radio" to do toggleplay command`

This toggles play/stop on the current selected station. Note: if there is no selected station then this command will have no effect.

LICENSE
-------

RadioMediaKeys is available under the MIT license.

Copyright (c) 2013 Het is Simpel (http://hetissimpel.nl) & Damien Glancy (http://damienglancy.ie)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.