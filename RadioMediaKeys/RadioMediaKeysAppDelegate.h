//
//  RadioAppDelegate.h
//  RadioMediaKeys
//
//  Created by Damien Glancy on 22/06/2013.
//  Copyright (c) 2013 Het is Simpel. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SPMediaKeyTap;

@interface RadioMediaKeysAppDelegate : NSObject <NSApplicationDelegate, NSMenuDelegate>

@property(strong) NSStatusItem *statusItem;
@property(weak) IBOutlet NSMenu *statusMenu;



@property (assign) IBOutlet NSWindow *window;
@property (nonatomic, strong) SPMediaKeyTap *keyTap;

- (IBAction)quitMenuItemPressed:(id)sender;

@end