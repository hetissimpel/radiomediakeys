//
//  main.m
//  RadioMediaKeys
//
//  Created by Damien Glancy on 22/06/2013.
//  Copyright (c) 2013 Het is Simpel. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
