//
//  RadioAppDelegate.m
//  RadioMediaKeys
//
//  Created by Damien Glancy on 22/06/2013.
//  Copyright (c) 2013 Het is Simpel. All rights reserved.
//

#import "RadioMediaKeysAppDelegate.h"
#import "SPMediaKeyTap.h"

@implementation RadioMediaKeysAppDelegate

#pragma mark - App Lifecycle

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    NSLog(@"[RadioMediaKeys] Started");
    
    _keyTap = [[SPMediaKeyTap alloc] initWithDelegate:self];
    
    if([SPMediaKeyTap usesGlobalMediaKeyTap])
		[_keyTap startWatchingMediaKeys];
	else
		NSLog(@"Media key monitoring disabled");
}

- (void)awakeFromNib {
    _statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
    _statusItem.menu = _statusMenu;
    _statusItem.image = [NSImage imageNamed:@"icon_32x32"];
    _statusItem.highlightMode = YES;
}

#pragma mark - Menu Items

- (IBAction)quitMenuItemPressed:(id)sender {
    [NSApp terminate:self];
}

#pragma mark - Key traps

- (void)mediaKeyTap:(SPMediaKeyTap*)keyTap receivedMediaKeyEvent:(NSEvent*)event {
	NSAssert([event type] == NSSystemDefined && [event subtype] == SPSystemDefinedEventMediaKeys, @"Unexpected NSEvent in mediaKeyTap:receivedMediaKeyEvent:");

	int keyCode = (([event data1] & 0xFFFF0000) >> 16);
	int keyFlags = ([event data1] & 0x0000FFFF);
	BOOL keyIsPressed = (((keyFlags & 0xFF00) >> 8)) == 0xA;
	
	if (keyIsPressed) {
        [self sendAppleEvent:keyCode];
	}
}

#pragma mark - AppleScript

- (void)sendAppleEvent:(int)keyCode {
    NSAppleScript *script;
    
    switch (keyCode) {
        case NX_KEYTYPE_PLAY:
            script = [[NSAppleScript alloc] initWithSource:@"tell application \"Radio\" to do toggleplay command"];
            break;
            
        /* FOR NEXT VERSION
        case NX_KEYTYPE_FAST:
            script = [[NSAppleScript alloc] initWithSource:@"tell application \"Radio\" to do next command"];
            break;
            
        case NX_KEYTYPE_REWIND:
            script = [[NSAppleScript alloc] initWithSource:@"tell application \"Radio\" to do previous command"];
            break;
         */
            
        default:
            break;
    }
    
    if (script) {
        [script executeAndReturnError:nil];
    }
}

@end